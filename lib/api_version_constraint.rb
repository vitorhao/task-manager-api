class ApiVersionConstraint
  def initialize(options)
    @version = options[:version]
    @default = options[:default]
  end

  def matches?(req) #com interrogacao indica que so retorta true ou false
    @default || req.headers['Accept'].include?("application/vnd.taskmanager.v#{@version}")
  end
end
